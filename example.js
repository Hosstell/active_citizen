steps = {
    'transport': {
        "https://www.gosuslugi.ru/category": {
            'func': (doc) =>  doc.getElementsByClassName('header-container')[6],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/category/pensions": {
            'func': (doc) =>  doc.getElementsByClassName('category-title')[3],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/15881": {
            'func': (doc) =>  doc.getElementsByClassName('service-text-link')[0],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/15881/1/info": {
            'func': (doc) => doc.getElementsByClassName('btn-frgu')[0],
            'arrowPosition': 'down'
        }
    },
    'relax': {
        "https://www.gosuslugi.ru/category": {
            'func': (doc) =>  doc.getElementsByClassName('header-container')[6],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/category/pensions": {
            'func': (doc) =>  doc.getElementsByClassName('category-title')[3],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/15881": {
            'func': (doc) =>  doc.getElementsByClassName('service-text-link')[1],
            'arrowPosition': 'down'
        },
        "https://www.gosuslugi.ru/15881/1/info": {
            'func': (doc) => doc.getElementsByClassName('btn-frgu')[0],
            'arrowPosition': 'down'
        }
    },
    'child': {
        "https://www.gosuslugi.ru/category": {
            'func': (doc) =>  doc.getElementsByClassName('header-container')[3],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/category/learning": {
            'func': (doc) =>  doc.getElementsByClassName('category-title')[0],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/group/kindergarten_enrollment": {
            'func': (doc) =>  doc.getElementsByClassName('service-text-link')[0],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/10999": {
            'func': (doc) => doc.getElementsByClassName('service-text-link')[0],
            'arrowPosition': 'up'
        },
        "https://www.gosuslugi.ru/10999/1": {
            'func': (doc) => doc.getElementsByClassName('btn-frgu')[0],
            'arrowPosition': 'down'
        }
    }
}

function addArrow (elem, arrowPosition) {
    elem.style.position = 'relative'
    elem.style.border = '2px dotted green'
    elem.style.borderRadius = '10px'
    elem.style.margin = '2px'

    let elemPosition = elem.getBoundingClientRect()

    if (arrowPosition === 'up') {
        var x = -100
        var y = elemPosition.width/2
    } else {
        var x = elemPosition.height + 30
        var y = elemPosition.width/2
    }

    var img = document.createElement("img");
    img.src = "https://upload.wikimedia.org/wikipedia/ru/a/a6/%D0%97%D0%B5%D0%BB%D1%91%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B5%D0%BB%D0%BA%D0%B0.png"
    img.style.transition = '1s linear'

    setTimeout(() => {
        setInterval(() => {
            img.style.top = `${x-40}px`
        }, 1000)
    }, 500)
    setInterval(() => {
        img.style.top = `${x}px`
    }, 1000)

    if (arrowPosition === 'up') {
        img.style.transform = 'rotate(180deg)'
    }

    img.style.position = 'absolute'
    img.style.height = '100px'
    img.style.width = '100px'
    img.style.left = `${y-50}px`
    img.style.top = `${x}px`
    setTimeout(() => {
        img.style.top = `${x-40}px`
    }, 0)
    img.zIndex = '1000'
    // document.body.appendChild(img)
    elem.appendChild(img)
    window.scrollTo({
        top: elemPosition.top - 400,
        behavior: "smooth"
    })
}

function setArrow () {
    let url = document.URL
    let get_element = steps[service][url]
    let btnText = get_element.func(document)
    try {
        addArrow(btnText, get_element.arrowPosition)
    } catch {
        setArrow()
    }

}
setTimeout(setArrow, 2000)

let url = document.URL
setInterval(() => {
    if (document.URL !== url) {
        url = document.URL
        setArrow()
    }
}, 2000)


var service = ''
chrome.storage.local.get('service', (data) => {
    console.log('service', data.service)
    service = data.service
    if (service) {
        var div = document.createElement("div");
        div.style.position = 'fixed'
        div.style.width = '300px'
        div.style.height = '100px'
        div.style.border = '2px solid blue'
        div.style.top = '55px'
        div.style.right = '5px'
        div.style.background = 'white'
        div.style.cursor = 'pointer'
        div.innerHTML = 'Убрать навигацию'
        div.style.textAlign = 'center'
        div.style.paddingTop = '34px'
        div.style.fontSize = '15pt'
        div.addEventListener('click', () => {
            chrome.storage.local.set({'service': ''})
            window.open(document.URL, '_self')
        })

        document.body.appendChild(div)
    }
})

localStorage.pluginStatus = true

