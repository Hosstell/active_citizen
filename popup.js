document.addEventListener('DOMContentLoaded', function() {
    var transportButton = document.getElementById('transport');
    transportButton.addEventListener('click', function() {
        chrome.storage.local.set({'service': 'transport'})
        window.open('https://www.gosuslugi.ru/category')
    })

    var relaxButton = document.getElementById('relax');
    relaxButton.addEventListener('click', function() {
        chrome.storage.local.set({'service': 'relax'})
        window.open('https://www.gosuslugi.ru/category')
    })

    var payButton = document.getElementById('child');
    payButton.addEventListener('click', function() {
        chrome.storage.local.set({'service': 'child'})
        window.open('https://www.gosuslugi.ru/category')
    })
}, false);